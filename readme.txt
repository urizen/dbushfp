DBusHFP
Hands Free Profile through D-BUS.
Released under GPL V3

This program aims to allow hands free operation of a phone through a car PC's touchscreen.  My carPC setup is simply a customized gnome 3 shell running programs that are pinned to the dock which allows me to switch them with a single touch.

With Bluez/Ofono/Pulseaudio properly configured, you don't even need this.  When every thing is setup correctly, phones calls are routed through the computer speakers and microphone input is sent to the phone.  This program simply shows call history, contacts, dial pad, a display of what is happening on the phone (call in progress, incoming call) and mostly to be able to control basic phone functions from the touchscreen.

When this program runs, when it detects a connected bluetooth device which has voicecalls capabalities, it downloads contacts and call history through an obex connection (on the session bus of dbus, i.e.: user session bus).  The program spawns the callback to process contacts and call history in a seperate process to prevent freezing of the gui.  Then when the user click on a call in the history or a phone number in the contacts it sends ofono the command to dial the number.  Buttons are also provided to accept/end calls.

At the moment, when a call is incoming the program window should request that the window manager raises it to the top.  It then switches to the "Call display" page.  I might modify this at some point to just show a notify with buttons.

There is a page for text messages with nothing in it.  When I get time to play with MAP protocol, I'll try to get a minimal text message interface working.

Todo:
 - Implement conference call
 - Add a call timer
 - Add contact picture display while in call
 - Add support for basic SMS (MMS might be another story...)

Known issues :
 - Sometimes the interface will hangs for a few seconds.  It seems to be while it's waiting for some reply after sending a message to ofono through dbus.  I've mostly experienced it while trying to end a call (the call will actually be ended when the program unfreezes).
 - It should work with multiple phones connected although behavior hasn't been totally tested.
 - I found sometimes you need to completely unpair the phone, reboot and then repair.  Particularly if the device keeps disconnecting for no apparent reasons.  I've seen that after a system update.
 - Probably some other things I didn't test.

#Requirements :
pulseaudio 12.2
bluez 5.50
ofono 1.21 (aur)
python 3
gtk 3

(Archlinux python packages)
python-pydbus 0.6.0
python-phonenumbers 8.9.10
python-vobject 0.9.6.1
... maybe others I forgot to document...

For the versions, it could work with older versions but these are what I have used.

### Setup
Before using this program, your phone should already be using your computer speakers and mic in a phone call.  This program is just a frontend that communicates through dbus to ofono/obex.
First get your phone working in A2DP bluetooth mode (music playing though computer speakers while connected).  Then the configs below might help getting HFP working.
At the moment, the user still needs write access to the program's folder for database storage.  Should move that to ~/.conf at some point.

## Pulseaudio
For my machine needed to force pulseaudio in ofono mode (can be auto, native or ofono).  Only setting headset=ofono gave me sound from the phone call (HFP/HSP mode working).
Find this line in /etc/pulse/default.pa :
load-module module-bluetooth-discover
modify for :
load-module module-bluetooth-discover headset=ofono

Also, some bluetooth adapters might need some extra steps to get them working. Consult this page: https://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User/Bluetooth/

#DBus Policy
Make sure your user is able to communicate with bluetoothd through dbus.  In Arch you need to be member of the "lp" group.
Hint : /etc/dbus-1/system.d/bluetooth.conf
  <!-- allow users of lp group (printing subsystem) to
       communicate with bluetoothd -->
  <policy group="lp">
    <allow send_destination="org.bluez"/>
  </policy>


## Other notes
These Pulseaudio settings are to investigate if it helps but from what I've read, pulseaudio should auto load needed modules compatible with the sink/source.

### Echo cancel and noise reduction
.ifexists module-echo-cancel.so
load-module module-echo-cancel aec_method=webrtc source_name=ec_out sink_name=ec_ref
set-default-source ec_out
set-default-sink ec_ref
.endif